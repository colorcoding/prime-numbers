package com.test.prime;

import java.util.ArrayList;
import java.util.List;

public class GetPrimeNumbers {

	
	public static void main(String[] args) {
		getPrimes(1,10000000);
	}
	
	public static boolean isNumDivisible(List<Long> primeList, long num) {
		boolean result = true;
		long count = ((num<10000)?(num/10):(num/100));
		if(primeList.size() == 0)
			return result;
		for (long i: primeList) {
			if(i>count)
				break;
			if(num % i == 0) {
				return false;
			}
		}
		return result;
	}
	
	public static void getPrimes(int n, long m) {
		long startTime = System.currentTimeMillis();
		List<Long> primeList = new ArrayList<>();
		for(long i=n; i<m+1; i++) {
			double sr = Math.sqrt(i); 
			if((i%2!=0) && (i%3!=0) && (i%5!=0) && (i%7!=0) && ((sr - Math.floor(sr)) != 0) && isNumDivisible(primeList,i)) {
				primeList.add(i);
			}
		}
		System.out.println("Time Elapsed: " + (System.currentTimeMillis() - startTime) + " msec");
		System.out.println(primeList.size());
	}
}
