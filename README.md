<h2>Prime Number Generator Code</h2>

Logic in Brief - A number is Prime if -
1. Its not divisible by 2,3,5 and 7
2. It is not a perfect square root
3. It is not divisible by any other Prime number

In this logic, every new Prime number found is added in the list, which is then used to work with condition 3 mentioned above 
(check if number is divisible by any other Prime)

The list keeps growing and with it grows the time complexity. To overcome this, there is a condition added in  code to avoid division check for an input number
to every other Prime in the list.

Comparing this method with Sieve of Erastothenes, and also checking the performance to generating prime numbers between 1-100 million

<h3>In Python</h3>

<table>
<tr>
<td>Input</td><td>Number of Primes</td><td>Time with this method</td><td>Time with Sieve</td><td>Time on Spark cluster with this method</td>
</tr>
<tr>
<td>1 million</td><td>78498</td><td>4.94 sec</td><td>40+ mins</td><td>TBD</td>
</tr>
<tr>
<td>10 million</td><td>664579</td><td>5 min, 15 sec</td><td>Code hung</td><td>TBD</td>
</tr>
<table>

<h3>In Java</h3>
<table>
<tr>
<td>Input</td><td>Number of Primes</td><td>Time with this method</td><td>Time with Sieve</td><td>Time on Spark cluster with this method</td>
</tr>
<tr>
<td>1 million</td><td>78498</td><td>0.2 sec</td><td>TBD</td><td>TBD</td>
</tr>
<tr>
<td>10 million</td><td>664579</td><td>3.1 sec</td><td>TBD</td><td>TBD</td>
</tr>
<tr>
<td>100 million</td><td>5761455</td><td>76 sec</td><td>TBD</td><td>TBD</td>
</tr>
<table>
<br>
<p>Java  is definitely faster than Python</p>

<h3>In C </h3>
<table>
<tr>
<td>Input</td><td>Number of Primes</td><td>Time with this method</td><td>Time with Sieve</td>
</tr>
<tr>
<td>1 million</td><td>78498</td><td>TBD</td><td>TBD</td>
</tr>
<tr>
<td>10 million</td><td>664579</td><td>TBD</td><td>TBD</td>
</tr>
<table>